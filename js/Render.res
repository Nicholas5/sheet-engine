open Webapi;

@bs.module("../js-modules/preact.module.js") external ce: (string, 'b, array<'c>) => 'd = "createElement"
@bs.module("../js-modules/preact.module.js") external preactRender: ('a, Dom.Element.t) => 'd = "render"
@bs.get external unsafeGetValue : ('a) => string = "value";

let rec renderSheet = (sheet: Sheet.sheet, rootElement) => {
  let renderValue = (label, tokens) => {
    switch Sheet.evaluate(tokens, sheet, ~debug=(Js.Array.includes(label, sheet.debug))) {
    | Ok(Number(ii)) =>
      ce("span", {"id": label ++ "-value-inner"}, [Belt.Int.toString(ii)])
    | Ok(Symbol(ss)) =>
      ce("span", {"id": label ++ "-value-inner"}, [ss])
    | Ok(NumberForm(label)) =>
      let value = Sheet.getNumberInput(sheet, label)
      let inputEventHandler = event => {
        Js.log(event)
        sheet.formNumberInputs -> Belt.HashMap.String.set(label, Sheet.toIntOrZero(Dom.InputEvent.data(event)))
        renderSheet(sheet, rootElement)
      }
      ce("input", {"id": label ++ "-value-inner", "type": "number", "value": Belt.Int.toString(value), "onInput": inputEventHandler}, [])
    | Ok(SetForm(label, setName)) =>
      let value = Sheet.getSymbolInput(sheet, label, setName)

      let selectOption = event => {
        Js.log(event)
        let value = event |> Dom.InputEvent.target |> unsafeGetValue
        Js.log(value)
        sheet.formSymbolInputs
        ->Belt.HashMap.String.set(
          label,
          value,
        )
        renderSheet(sheet, rootElement)
      }

      let options = switch Belt.HashMap.String.get(sheet.formulas, setName) {
      | Some(Set(symbols)) =>
        symbols -> Belt.Array.map(symbol => {
          if value == symbol {
            ce("option", {"id": label ++ "-form-input-" ++ symbol, "value": symbol, "selected": ""}, [symbol])
          } else {
            ce("option", {"id": label ++ "-form-input-" ++ symbol, "value": symbol}, [symbol])
          }
        })
      | _ =>
        Js.log("Error: no such set: " ++ setName)
        []
      }

      ce("select", {"id": label ++ "-value-inner", "onInput": selectOption}, options)
    | Ok(_) =>
      ce("div", {"id": label ++ "-value-inner"}, ["Err: Impossible! Unexpected evaluate result"])
    | Error(str) =>
      ce("div", {"id": label ++ "-value-inner"}, ["Err: " ++ str])
    }
  }

  let rec renderBlock = (blockName, blockClasses, subItems) => {
    let classString = Js.Array.joinWith(" ", blockClasses)
    let blockContent = ce("div", {"id": "content-" ++ blockName, "class": classString}, renderLayout(subItems))
    let blockChildren = if blockName == "" {
      [blockContent]
    } else {
      [
        ce("h1", {"id": "header-" ++ blockName}, [blockName]),
        blockContent,
      ]
    }
    ce("section", {"id": "section-" ++ blockName}, blockChildren)
  }

  and renderLayout = (layout: array<Sheet.layout>) => {
    layout -> Belt.Array.keepMap((item) =>
      switch item {
      | Block(blockName, blockClasses, subItems) =>
        if blockName == "_" {
          None
        } else {
          Some(renderBlock(blockName, blockClasses, subItems))
        }
      | Label(label, displayLabel) =>
        switch Belt.HashMap.String.get(sheet.formulas, label) {
        | Some(Variable(tokens)) =>
            Some(ce("div", {"id": label, "class": "variable"}, [
              ce("div", {"id": label ++ "-label", "class": "label"}, [displayLabel->Belt.Option.getWithDefault(label)]),
              ce("div", {"id": label ++ "-value", "class": "value"}, renderValue(label, tokens))
            ]))
        | Some(Set(symbols)) => None
        | None =>
          Js.log("Impossible! Missing label " ++ label)
          None
        }
      }
    )
  }

  let inputBuffer = ref(Source.source)
  let onSourceUpdate = (event) => {
    inputBuffer := event |> Dom.InputEvent.target |> unsafeGetValue
  }
  let onParseClick = (event) => {
    switch Sheet.parse(inputBuffer.contents) {
    | Ok(sheet) => renderSheet(sheet, rootElement)
    | Error(msg) => Js.log("Err: " ++ msg)
    }
  }

  let bla = ce("div", {"id": "sourceRoot"}, [
    ce("textarea", {"wrap": "off", "style": "width: 100%; height: 50ex;", "onInput": onSourceUpdate}, [Source.source]),
    ce("input", {"id": "sourceButton", "type": "button", "value": "Parse", "onClick": onParseClick}, []),
    ce("div", {"id": "sheetRoot"}, renderLayout(sheet.layout)),
  ])

  preactRender(bla, rootElement)
}

