#!/bin/bash
set -euo pipefail
yarn
/bin/bash ./build.sh

mkdir -p public/js-modules
cp html/index.html public/
cp -r html/assets public/
cp -r lib/es6_global/js public/
cp -r node_modules public/

cp node_modules/preact/dist/preact.module.js public/js-modules/
cp node_modules/preact/dist/preact.module.js.map public/js-modules/
