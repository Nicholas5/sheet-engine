open Webapi

switch Dom.Document.getElementById("root", Dom.document) {
| None => Js.log("Error: No root element")
| Some(rootElement) =>
  Render.renderSheet(Sheet.newSheet(0), rootElement)
}
